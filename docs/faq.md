## How can I install kite in Mac OS X?
 you can follow this complicated recipe

## Can I get involved in development?

yes

## Can I use KITE?

YES

#How to cite:

```
@ARTICLE{Joao2020-db,
  title     = "{KITE}: high-performance accurate modelling of electronic
               structure and response functions of large molecules, disordered
               crystals and heterostructures",
  author    = "Jo{\~a}o, Sim{\~a}o M and An{\dj}elkovi{\'c}, Mi{\v s}a and
               Covaci, Lucian and Rappoport, Tatiana G and Lopes, Jo{\~a}o M V
               P and Ferreira, Aires",
  publisher = "The Royal Society",
  volume    =  7,
  number    =  2,
  pages     = "191809",
  month     =  feb,
  year      =  2020,
  keywords  = "Chebyshev expansions; disorder; electronic structure; optical
               response; quantum transport; tight-binding simulations",
  language  = "en"
}
```

